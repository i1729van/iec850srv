# -*- coding: utf-8 -*-
"""
    srv850.__main__

"""

import asyncio
import logging
import logging.config
import argparse
import signal
from src.srv850.server import Server850
from src.srv850.model import ServerModelFromScl


class AppConfig:
    """ Config object for App """

    def __init__(self, event_loop, file='hsd.scd'):
        self.loop = event_loop
        self.file = file
        self.ied_name = None
        self.logger = None


class App():
    def __init__(self, config):
        self.logger = config.logger
        self._server = None
        self._config = config
        self._loop = config.loop

    def signal_handler(self, sig, frame):
        self._loop.close()
        print("You re pressed CTRL+C")

    def start(self):
        self.logger.debug("start app")
        self._start_server()

    def _start_server(self):
        server_model = ServerModelFromScl(self._config).model
        self._server = Server850(server_model)
        self._server.start()

    async def stop(self):
        """ Останов """
        try:
            self.logger.debug("stop app")
            await self._server.stop()
        except:
            pass
        try:
            self.logger.debug("loop stop")
            await self._loop.stop()
        except:
            pass


def main():

    parser = argparse.ArgumentParser(description='Volcano IEC61850 Server')
    logging.config.fileConfig(
        fname='logging.conf', disable_existing_loggers=False)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    parser.add_argument('--core_host', help='Volcano core host',
                        default='volcano')
    parser.add_argument('--ied_name', help='provide an IED name',
                        default='TEMPLATE')
    parser.add_argument('--file', dest='sclfile', default='hsr.scd',
                        help='scd/icd/cid file', )
    args = parser.parse_args()
    loop = asyncio.get_event_loop()
    cfg = AppConfig(loop, args.sclfile)
    cfg.ied_name = args.ied_name
    cfg.logger = logger
    logger.debug("volcano_core host port: {0}".format(args.core_host))
    app = App(cfg)
    signal.signal(signal.SIGINT, app.signal_handler)
    app.start()
    logger.debug('This is a debug message')
    loop.run_forever()


if __name__ == '__main__':
    main()

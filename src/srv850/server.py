import logging
import sys
import iec61850
import time

tcpPort = 102

running = 1


class Server850():
    def __init__(self, model=None):
        self._model = iec61850.IedModel_create("testmodel")
        lDevice1 = iec61850.LogicalDevice_create("SENSORS", self._model)
        lln0 = iec61850.LogicalNode_create("LLN0", lDevice1)
        ttmp1 = iec61850.LogicalNode_create("TTMP1", lDevice1)
        iec61850.CDC_SAV_create(
            "TmpSv",  iec61850.toModelNode(ttmp1), 0, False)
        iec61850.CDC_ASG_create(
            "TmpSp",  iec61850.toModelNode(ttmp1), 0, False)

        self._logger = logging.getLogger(__name__)
        self._ied_server = iec61850.IedServer_create(self._model)

    def start(self):
        self._logger.debug("Start Server 61850")
        iec61850.IedServer_start(self._ied_server, tcpPort)
        if not(iec61850.IedServer_isRunning(self._ied_server)):
            self._logger.debug("Starting server failed! Exit.\n")
            iec61850.IedServer_destroy(self._ied_server)
            sys.exit(-1)

    def __del__(self):
        iec61850.IedServer_stop(self._ied_server)
        iec61850.IedServer_destroy(self._ied_server)
        iec61850.IedModel_destroy(self._model)
        print("del server")

import iec61850
import logging
from lxml import objectify


class ServerModelFromScl():
    def __init__(self, config):
        self.file = config.file
        self.ied_name = config.ied_name
        self.logger = logging.getLogger(__name__)
        self._root = objectify.parse(self.file).getroot()
        if not self.ied_name in [ied.get("name") for ied in self._root.IED]:
            raise ValueError("Ied not in SCL file")

        ied_node = [ied for ied in self._root.IED if ied.get(
            'name') == config.ied_name]
        print(self._get_ld_list_from_ied(ied_node[0]))
        self.model = iec61850.IedModel_create(name=self.ied_name)
        lDevice1 = iec61850.LogicalDevice_create("SENSORS", self.model)
        lln0 = iec61850.LogicalNode_create("LLN0", lDevice1)
        ttmp1 = iec61850.LogicalNode_create("TTMP1", lDevice1)
        iec61850.CDC_SAV_create(
            "TmpSv",  iec61850.toModelNode(ttmp1), 0, False)
        iec61850.CDC_ASG_create(
            "TmpSp",  iec61850.toModelNode(ttmp1), 0, False)

    def _get_ld_list_from_ied(self, ied_node):
        return [ld for ld in ied_node.AccessPoint.Server.iterchildren() if ld.get('inst')]

    def __del__(self):
        if hasattr(self, 'model') and self.model is not None:
            # iec61850.IedModel_destroy(self.model)
            self.logger.debug("destroy ServerModel")

# Python 61850 Server

[![N|Solid](https://avatars2.githubusercontent.com/u/25686380?s=200&v=4)](https://nodesource.com/products/nsolid)

# Порядок установки
  - проверить/собрать необходимую c-библиотеку
  - установить python-зависимости

### Requirements

сервер использует след. пакеты:

* [lib61850] - C-реализация библиотеки 61850 от mz-Automation
* [LXML] - the most feature-rich and easy-to-use library for processing XML.

Пакеты и их версии доступны в файле requirements.txt.




[//]: # ( )
   [lib61850]: <https://github.com/mz-automation/libiec61850>
   [LXML]: <https://lxml.de/>